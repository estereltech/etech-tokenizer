#include "tok.h"

#include <string.h>
#include <stdio.h>


//// Privé.


struct sttok
{

	// Le tableau de poiteurs vers chaînes de caractères.
	char **array;

	// Le nombre d'éléments du tableau.
	size_t size;
};

/*
 * Cette fonction recherche les occurences du séparateur afin de déterminer
 * le nombre d'éléments.
 * Nombre d'éléments = nombre de séparateurs + 1. Cela permet d'allouer de la
 * mémoire pour le tableau par un seul appel à malloc.
 * @str : la chaîne à traiter
 * @sep : la chaîne motif séparateur
 */
static size_t
_count_seps(const char *str, const char *sep)
{
	size_t nb = 0, sz = strlen(sep);
	char *ptr = (char*)str, *found = NULL;

	/*
	 * Si un séparateur est trouvé nb est incrémenté et le curseur ptr est
	 * avancé du nombre d'octets composant le séparateur.
	 */
	while( (found = strstr(ptr, sep)) ) {

		// Incrémentation du nombre de séparateurs.
		nb++;

		// Avancer le curseur/pointeur après le séparateur trouvé.
		ptr += (found - ptr) + sz;
	}

	return nb;
}

// Utilitaire pour libérer les ressources allouées pour le tableau.
static void
_free_array(struct sttok *t)
{
	if ( t == NULL || t->array == NULL ) {
		return;
	}

	for (size_t i = 0; i < t->size; ++i) {
		free(t->array[i]);
	}

	free(t->array);

	t->array = NULL;
	t->size = 0;
}


//// Public.


struct sttok*
tok_new(void)
{
	struct sttok *t = NULL;
	t = (struct sttok*)malloc(sizeof(*t));
	if ( t != NULL ) {
		t->array = NULL;
		t->size = 0;
	}

	return t;
}

void
tok_free(struct sttok *t)
{
	if ( t == NULL ) {
		return;
	}

	_free_array(t);

	free(t);
	t = NULL;
}

bool
tok_tokenize(struct sttok *t, const char *str, const char *sep)
{
	if ( str == NULL || sep == NULL ) {
		return false;
	}

	/*
	 * Calcul du nombre de fois où le séparateur est trouvé dans la chaîne.
	 * Note : il faut ajouter 1 pour avoir le nombre d'éléments.
	 * Cela permet d'allouer de la mémoire pour la liste en une seule passe.
	 */
	size_t nb = _count_seps(str, sep);
	if ( nb == 0 ) {
		return false;
	}

	/*
	 * Copier la chaîne originale dans un bloc mémoire alloué par alloca afin
	 * de ne pas l'altérer par la fonction strtok_r.
	 * Note : ne pas libérer par 'free' le bloc mémoire.
	 */
	size_t s = strlen(str);
	char *pstr = (char*)alloca((s + 1) * sizeof(*pstr));
	if ( NULL == pstr ) {
		return false;
    }
    void *v = memcpy(pstr, str, s);
    if ( NULL == v ) {
		return false;
	}
	pstr[s] = 0x00;

	// Allocation de mémoire pour le tableau en une fois.
	t->array = (char**)malloc((nb + 1) * sizeof(*t->array));
	if ( t->array == NULL ) {
        return false;
    }

    char *tok 	= NULL;
    char *st 	= NULL;		// Chaîne de stockage.

	/*
	 * Séparation des éléments en tableau.
	 * Note : t->size est initialisé à 0 dans le constructeur.
	 */
    tok = strtok_r(pstr, sep, &st);
    while(tok != NULL) {
        t->array[t->size] = strdup(tok);
        if ( NULL == t->array[t->size] ) {
            _free_array(t);

            return false;
        }

        // printf("st = %s\n", st);

        // Incrémentation du nombre d'éléments.
        t->size++;

        tok = strtok_r(NULL, sep, &st);
    }

    return true;
}

/*
 * Note : cette fonction retourne le tableau ainsi que le nombre d'éléments par
 * l'argument *size (optionnel - peut être NULL).
 */
char**
tok_get_array(struct sttok *t, size_t *size)
{
	if ( size != NULL ) {
		*size = t->size;
	}

    return t->array;
}

// Retourne le nombre d'éléments du tableau.
size_t
tok_get_size(struct sttok *t)
{
	return t->size;
}

