/*

	Projet    : Module tok[h,c] incorporant 'strtok_r'.
	Auteur    : © Philippe Maréchal
	Date      : 08 avril 2015
	Version   :

	Notes     :

	Révisions :

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "tok.h"


#define __unused 	__attribute__((unused))
#define __used 		__attribute__((used))


int
main(int __unused argv, char __unused **argc)
{
	printf("\nDébut du programme.\n\n");

	const char str[] =
		"Avant premier\r\nPremier\r\nSecond\r\nTroisième\r\nQuatrième\r\n"
		"Cinquième\r\nSixième\r\nSeptième";
	const char sep[] = "\r\n";
	
	Tokens t = tok_new();
	if ( t == NULL ) {
		return EXIT_FAILURE;
	}

	if ( ! tok_tokenize(t, str, sep) ) {
		tok_free(t);
		
		return EXIT_FAILURE;
	}

	// Retrouver le tableau et le nombre d'éléments.
	size_t sz = 0;
	char **array = tok_get_array(t, &sz);

	if ( array && sz > 0 ) {
		for (size_t i = 0; i < sz; ++i) {
			printf("%s\n", array[i]);
		}
	}

	tok_free(t);

	printf("\n\n");

	// Tentative de provocation d'une erreur.
	const char str2[] = "Avant premier||Premier";
	const char sep2[] = "||";

	Tokens t2 = tok_new();
	if ( t2 == NULL ) {
		return EXIT_FAILURE;
	}

	if ( ! tok_tokenize(t2, str2, sep2) ) {
		tok_free(t2);
		
		return EXIT_FAILURE;
	}

	size_t sz2 = 0;
	char **array2 = tok_get_array(t2, &sz2);

	if ( array2 && sz2 > 0 ) {
		for (size_t i = 0; i < sz2; ++i) {
			printf("%s\n", array2[i]);
		}
	}

	tok_free(t2);

	// strtok_r.
	printf("\nstrtok_r + alloca :\n\n");
	char *str3 =
		"Avant premier\r\nPremier\r\nSecond\r\nTroisième\r\nQuatrième\r\n"
		"Cinquième\r\nSixième\r\nSeptième";
	char *buf = (char*)alloca(strlen(str3) + 1);
	strcpy(buf, str3);
	const char sep3[] = "\r\n";
	char *s;
	char *svptr;
	s = strtok_r(buf, sep3, &svptr);
	while(s != NULL ) {
		printf("s = %s\n", s);
		s = strtok_r(NULL, sep3, &svptr);
	}
	
	printf("\nFin du programme.\n\n");

	return EXIT_SUCCESS;
}

