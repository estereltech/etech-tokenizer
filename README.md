**Module C tokenizer[h,c]**  

Éclatement d'une chaîne en un tableau selon un motif séparateur.  
Un 'Makefile' simple est fourni pour la compilation avec gcc.  

*Pour construire le programme en mode optimisé :*  
$ make

*Pour construire le programme en mode de débogage :*  
$ make debug

*Pour exécuter le programme :*  
$ ./tokenizer
