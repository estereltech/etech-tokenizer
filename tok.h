#pragma once


#include <stdlib.h>
#include <stdbool.h>


/*

	Projet    : Éclatement d'une chaîne en un tableau selon un motif séparateur.
	Auteur    : © Philippe Maréchal
	Date      : 08 avril 2015
	Version   : 1.1

	Notes     : Compiler avec -D_GNU_SOURCE (pour strtok_r).

	Révisions :
	08 décembre 2016 > nouvelle fonction 'count_seps'.
	09 décembre 2016 > la chaîne originale n'est pas modifiée.
	27 mars 2017 > remplacement de while(1) par do...while dans 'count_seps'.
	30 mars 2017 > modifications mineures et version 1.0.
	03 mai 2017 > modification / simplification de la fonction '_count_seps'.
	04 mai 2017 > nouvelles fonction utilitaire 'tok_get_size'.
	16 mai 2018 > utilsation de 'alloca' dans 'tok_tokenize' et version 1.1.
*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


typedef struct sttok *Tokens;

Tokens
tok_new(void);
void
tok_free(Tokens);

bool
tok_tokenize(Tokens, const char *str, const char *sep);
char**
tok_get_array(Tokens, size_t *size);
size_t
tok_get_size(Tokens);

