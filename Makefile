#
# Makefile personnalisé utilisé avec Geany et/ou un terminal.
#

# Shell par défaut.
SHELL=/bin/sh

# Compilateur.
CC?=cc

# Fichiers source.
SRCS=$(wildcard *.c)

# Nom de l'exécutable.
APP_NAME=tokenizer

# Fichier de sortie.
OUT=-o $(APP_NAME)

# Optimisé, sans information de débogage.
CRELEASE=-DNDEBUG -O3 -s

# Débogage
CDEBUG=-Og -g

# Options de compilation.
CFLAGS=-std=gnu11 -march=native -Wall -Werror -Wextra -D_GNU_SOURCE

#Inclusions et liaisons.
CPPFLAGS=
LDFLAGS=

# Cibles ne correspondant à aucun fichier du répertoire en cours.
.PHONY: release debug clean clobber

# Cible par défaut
.DEFAULT_GOAL=release


release: $(SRCS)
	$(CC) $(CRELEASE) $(CFLAGS) $(OUT) $^

debug: $(SRCS)
	$(CC) $(CDEBUG) $(CFLAGS) $(OUT) $^

# Nettoyage.
clean:
	$(RM) *.h~
	$(RM) *.c~
	$(RM) Makefile~

clobber: clean
	$(RM) $(APP_NAME)
